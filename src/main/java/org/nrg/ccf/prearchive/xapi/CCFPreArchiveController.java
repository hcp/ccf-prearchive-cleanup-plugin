/**
 * 
 */
package org.nrg.ccf.prearchive.xapi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.ccf.prearchive.utils.QueryService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xdat.rest.AbstractXapiRestController;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The Class CCFPreArchiveController.
 * 
 * @author Atul
 */
@SuppressWarnings("deprecation")
@XapiRestController
@RequestMapping(value = "/preArchiveDataIssues")
@Api(description = "CCF Pre Archive API")
public class CCFPreArchiveController extends AbstractXapiRestController {

	/** The logger. */
	public static Logger logger = Logger.getLogger(CCFPreArchiveController.class);

	/** The query svc. */
	QueryService querySvc = new QueryService();

	/**
	 * Instantiates a new CCF pre archive controller.
	 *
	 * @param userManagementService
	 *            the user management service
	 * @param roleHolder
	 *            the role holder
	 */
	@Autowired
	protected CCFPreArchiveController(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}

	/**
	 * Gets the all pre archive record counts.
	 *
	 * @param hours
	 *            the hours
	 * @return the all pre archive record counts
	 */
	@ApiOperation(value = "Returns Pre Archive Session counts", response = String.class)
	@ApiResponses({
			@ApiResponse(code = 200, message = "Successfully fetched the count for sessions in building/deletion state."),
			@ApiResponse(code = 403, message = "Insufficient permissions to get the count value."),
			@ApiResponse(code = 500, message = "An unexpected error occurred.") })
	@RequestMapping(value = "/getCounts/{hours}", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> getAllPreArchiveRecordCounts(@PathVariable("hours") Integer hours) {
		final UserI user = getSessionUser();
		try {
			if (Roles.isSiteAdmin(user)) {
				return new ResponseEntity<String>(querySvc.getAllPreArchiveRecordCounts(hours).toString(),
						HttpStatus.OK);
			} else {
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return getErrorMessage(e);
		}
	}

	/**
	 * Update pre archive records.
	 *
	 * @param hours
	 *            the hours
	 * @return the response entity
	 */
	@ApiOperation(value = "Resolve Pre Archive session data issues", response = String.class)
	@ApiResponses({
			@ApiResponse(code = 200, message = "Successfully updated status of sessions in building/deletion state."),
			@ApiResponse(code = 403, message = "Insufficient permissions to update data."),
			@ApiResponse(code = 500, message = "An unexpected error occurred.") })
	@RequestMapping(value = "/resolveData/{hours}", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.POST)
	public ResponseEntity<String> updatePreArchiveRecords(@PathVariable("hours") Integer hours) {
		final UserI user = getSessionUser();
		logger.info(user.getUsername() + " tried to resolve Pre Archive sessions on "
				+ new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss").format(new Date()));
		try {
			if (Roles.isSiteAdmin(user)) {
				return new ResponseEntity<String>(
						String.format("%d records updated in the database.", querySvc.updatePreArchiveRecords(hours)),
						HttpStatus.OK);
			} else {
				logger.info("Operation failed due to insufficient privileges.");
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return getErrorMessage(e);
		}
	}

	/**
	 * Show data.
	 *
	 * @param hours
	 *            the hours
	 * @return the response entity
	 */
	@ApiOperation(value = "Display PreArchive session having issues.", response = String.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Successfully displayed data on UI."),
			@ApiResponse(code = 403, message = "Insufficient permissions to view data."),
			@ApiResponse(code = 500, message = "An unexpected error occurred.") })
	@RequestMapping(value = "/showData/{hours}", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, Object>>> showData(@PathVariable("hours") Integer hours) {
		final UserI user = getSessionUser();
		try {
			if (Roles.isSiteAdmin(user)) {
				return new ResponseEntity<List<Map<String, Object>>>(
						querySvc.getAllPreArchiveRecordsDataAsJSON(user, hours), HttpStatus.OK);
			} else {
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Gets the error message.
	 *
	 * @param e
	 *            the e
	 * @return the error message
	 */
	private ResponseEntity<String> getErrorMessage(Exception e) {
		logger.error(e);
		e.printStackTrace();
		if (e instanceof InsufficientPrivilegesException)
			return new ResponseEntity<>("ERROR: " + e.getMessage(), HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>("ERROR: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
