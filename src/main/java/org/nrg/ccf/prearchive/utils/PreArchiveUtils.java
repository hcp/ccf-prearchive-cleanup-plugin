/**
 * 
 */
package org.nrg.ccf.prearchive.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * The Class PreArchiveUtils.
 *
 * @author Atul
 */
public class PreArchiveUtils {

	private static final String QUERIES_PROPERTIES_FILE = "preArchiveQueries.properties";

	/** The properties. */
	private static Properties properties = null;

	/**
	 * Loads the properties file.
	 *
	 * @param fileName
	 *            the file Name.
	 * @throws IOException
	 *             the exception
	 */
	private static void init(String fileName) throws IOException {

		properties = new Properties();
		properties.load(PreArchiveUtils.class.getClassLoader().getResourceAsStream(fileName));

	}

	/**
	 * Gets the value.
	 *
	 * @param propertyName
	 *            the property name
	 * @return the value
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getValue(String propertyName) throws IOException {
		if (properties == null) {
		init(QUERIES_PROPERTIES_FILE);
		}
		return (String) properties.get(propertyName);
	}

}
