/**
 * 
 */
package org.nrg.ccf.prearchive.configuration;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

// TODO: Auto-generated Javadoc
/**
 * The Class CcfPreArchivePlugin.
 *
 * @author Atul
 */
@XnatPlugin(value = "ccfPreArchivePlugin", name = "CCF Pre Archive Plugin")
@ComponentScan({ "org.nrg.ccf.prearchive.xapi", "org.nrg.ccf.prearchive.utils" })
public class CcfPreArchivePlugin {
	/** The logger. */
	public static Logger logger = Logger.getLogger(CcfPreArchivePlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public CcfPreArchivePlugin() {
		logger.info("Configuring CCF Pre Archive Session plugin");
	}

}