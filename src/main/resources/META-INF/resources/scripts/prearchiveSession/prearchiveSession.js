/*!
 * XNAT Task site settings functions
 */
var XNAT = getObject(XNAT);

(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        return factory();
    }
}(function() {

    var preArchiveConf;
    XNAT.admin =
        getObject(XNAT.admin);

    XNAT.admin.preArchiveConf = preArchiveConf =
        getObject(XNAT.admin.preArchiveConf);

    preArchiveConf.resolveData = function() {
		var hours=document.getElementById('hours').value;
		var res=true;
		if(hours < 4)
		{
			res=confirm('You\'ve requested to update recent records.  Session building and session deletion can take several hours.\n\n' + 
					'If your request returned any very recent records, care should be taken to not change the status of running ' + 
					'processes, but removal of recent records may be appropriate if there was a recent web service restart.\n\n' +
					'Are you sure you wish to continue?');
		}
		if(res)
		{
			XNAT.xhr.post({
				url: XNAT.url.restUrl('/xapi/preArchiveDataIssues/resolveData/'+hours),
				success: function(data) {
					XNAT.ui.banner.top(2000, data, 'success');
					preArchiveConf.clearConflictedDataTable();
					$("#resolvebtnDiv").attr("style","display:none");
					preArchiveConf.showData();
				},
				error: function() {
					XNAT.ui.banner.top(3000, '<b>Unable to update records. Kindly check logs for errors.', 'error', '600px');
				}
			})
		}
    }

    preArchiveConf.showData = function() {
        preArchiveConf.clearConflictedDataTable();
        var conflictTable = XNAT.table({
            className: 'xnat-table compact',
			style: {
                width: '100%'
			}
        });
		conflictTable.tr();
        conflictTable
            .th('Project')
            .th('Subject')
            .th('Session Name')
            .th('Uploaded')
            .th('Last Modified')
            .th('Status');
        preArchiveConf.result(conflictTable);
    }

    preArchiveConf.clearConflictedDataTable= function() {
        $("#conflicted-data-table").empty();
    }
	
    preArchiveConf.result= function(conflictTable) {
		var hours=document.getElementById('hours').value;
        XNAT.xhr.get({
            url: XNAT.url.restUrl('/xapi/preArchiveDataIssues/showData/'+hours),
            success: function(data) {
                XNAT.ui.banner.top(1500, 'Loading data.', 'success');
                var allRows = data.map(function(item, i) {
					var date=new Date(item.last_modified);
					var lastMod=preArchiveConf.localDate(date)+" "+preArchiveConf.localTime(date);
					if (item.hasOwnProperty('uploaded')) {
						var uploadDate=new Date(item.uploaded);
						var uploaded=preArchiveConf.localDate(uploadDate)+" "+preArchiveConf.localTime(uploadDate);
					}

                    return [
                        item.project,
                        item.subject, item.name, uploaded, lastMod, item.status
                    ];
                });
                conflictTable.rows(allRows);

                if (data.length) {
                    $("#conflicted-data-table").append(conflictTable.table);
					$("#conflicted-data-table").find("tr:odd").css("background-color", "##F2F2F2");
					$("#conflicted-data-table").find("tr:even").css("background-color", "#D6D6D6");
					$("#resolvebtnDiv").attr("style","display:block");
                } else {
                	$("#resolvebtnDiv").attr("style","display:none");
                    $("#conflicted-data-table").spawn('p', '<b>There are no sessions in Building/Deletion state for the time period specified.</b>')
                }
            },
            error: function() {
                XNAT.ui.banner.top(3000, '<b>Unable to load data. Kindly check logs for errors.', 'error', '600px');
            }
        })
    }
    preArchiveConf.localDate=function(date){
		return date.toLocaleDateString()
	}

	preArchiveConf.localTime =function(date){
		return date.toLocaleTimeString()
	}
	
	preArchiveConf.validateValueAndShowData = function(){
		if(!$('#hours').val().match(/^\d+$/) || $('#hours').val()<=0)
		{
			alert("Please enter an integer value greater than zero.");
			$('#hours').val(12);
		}
		else
		{
			preArchiveConf.showData();
		}
	}
    XNAT.admin.preArchiveConf = preArchiveConf;
}));
